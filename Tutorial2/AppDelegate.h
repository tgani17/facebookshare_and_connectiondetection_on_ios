//
//  AppDelegate.h
//  Tutorial2
//
//  Created by Steven Gani on 12/17/15.
//  Copyright © 2015 Steven Gani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

