//
//  AppDelegate.m
//  Tutorial2
//
//  Created by Steven Gani on 12/17/15.
//  Copyright © 2015 Steven Gani. All rights reserved.
//

#import "AppDelegate.h"
#import "MyReachability.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    MyReachability *reachability = [MyReachability reachabilityWithHostname:@"www.google.com"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No network connection"
                                                    message:@"You must be connected to the internet to use this app."
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];

    
    reachability.reachableBlock = ^(MyReachability *reachability) {
        [alert dismissWithClickedButtonIndex:0 animated:true];
        NSLog(@"Network is reachable.");
    };
    
    reachability.unreachableBlock = ^(MyReachability *reachability) {
        [alert show];
        NSLog(@"Network is unreachable.");
    };
    
    // Start Monitoring
    [reachability startNotifier];
    
//    // Initialize View Controller
//    self.viewController = [[ViewController alloc] initWithNibName:@"MyViewController" bundle:nil];
//    
//    // Initialize Window
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//
//    // Configure Window
//    [self.window setRootViewController:self.viewController];
//    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
