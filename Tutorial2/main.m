//
//  main.m
//  Tutorial2
//
//  Created by Steven Gani on 12/17/15.
//  Copyright © 2015 Steven Gani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
